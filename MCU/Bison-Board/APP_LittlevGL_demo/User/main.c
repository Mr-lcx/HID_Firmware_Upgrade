/*
*******************************************************************************************************
*
* 文件名称 : main.c
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : 主函数文件
* 
*******************************************************************************************************
*/



/* 头文件 -----------------------------------------------------------*/
#include "bsp.h"


/* 宏定义 -----------------------------------------------------------*/


/* 变量 -------------------------------------------------------------*/


/* 函数声明 ---------------------------------------------------------*/
void lv_tutorial_hello_world(void);

extern void lv_test_theme_1(lv_theme_t *th);


/**
  * @brief main主函数
  * @param None
  * @retval	None
  */
int main(void)
{
	SCB->VTOR = FLASH_BASE | 0x10000;//设置偏移量
	
	uint16_t tick;
	
	bsp_init();
	
	ltdc_clear_layer(LCD_COLOR_BLACK,0);
	
	//demo_create();
	
	//lv_test_theme_1(lv_theme_night_init(210, NULL));
	//lv_test_theme_1(lv_theme_alien_init(210, NULL));
	lv_test_theme_1(lv_theme_material_init(10,NULL));
	
	//lv_tutorial_hello_world();
	
	
	while(1)
	{

		lv_task_handler();
		if(tick++>50)
		{
			led_toggle();
			tick=0;
		}
		HAL_Delay(10);
	}
	
	
}


/**
 * Create a simple 'Hello world!' label
 */
void lv_tutorial_hello_world(void)
{
    lv_obj_t * scr = lv_disp_get_scr_act(NULL);     /*Get the current screen*/

    /*Create a Label on the currently active screen*/
    lv_obj_t * label1 =  lv_label_create(scr, NULL);

    /*Modify the Label's text*/
    lv_label_set_text(label1, "Hello world!");

    /* Align the Label to the center
     * NULL means align on parent (which is the screen now)
     * 0, 0 at the end means an x, y offset after alignment*/
    lv_obj_align(label1, NULL, LV_ALIGN_CENTER, 0, 0);
}


/*
		key = key_get();
		switch(key)
		{
			case KEY_LEFT_DOWN:
				printf("KEY_LEFT_DOWN\r\n");
				break;
			case KEY_LEFT_UP:
				printf("KEY_LEFT_UP\r\n");
				break;
			case KEY_LEFT_LONG:
				printf("KEY_LEFT_LONG\r\n");
				break;
			case KEY_WKUP_DOWN:
				printf("KEY_WKUP_DOWN\r\n");
				break;
			case KEY_WKUP_UP:
				printf("KEY_WKUP_UP\r\n");
				break;
			case KEY_WKUP_LONG:
				printf("KEY_WKUP_LONG\r\n");
				break;
			case KEY_RIGHT_DOWN:
				printf("KEY_RIGHT_DOWN\r\n");
				break;
			case KEY_RIGHT_UP:
				printf("KEY_RIGHT_UP\r\n");
				break;
			case KEY_RIGHT_LONG:
				printf("KEY_RIGHT_LONG\r\n");
				break;
			default:
				break;
		}
*/



/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/
