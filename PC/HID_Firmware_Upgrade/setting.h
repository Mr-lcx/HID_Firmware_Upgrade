#ifndef SETTING_H
#define SETTING_H

#include <QObject>
#include <QObject>
#include <QSettings>
#include <QFile>
#include <QDir>

class Setting: public QObject
{
    Q_OBJECT
public:
    Setting(QObject *parent = 0);

    void writeSettings(const QString &key,const QString &value);

    void removeSettings(const QString &key);

    QString readSettings(const QString &key);
};

#endif // SETTING_H
