/********************************************************************************
** Form generated from reading UI file 'dialog_help.ui'
**
** Created by: Qt User Interface Compiler version 5.9.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG_HELP_H
#define UI_DIALOG_HELP_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextBrowser>

QT_BEGIN_NAMESPACE

class Ui_Dialog_Help
{
public:
    QGridLayout *gridLayout;
    QTextBrowser *textBrowser;
    QLabel *label;
    QPushButton *pushButton;

    void setupUi(QDialog *Dialog_Help)
    {
        if (Dialog_Help->objectName().isEmpty())
            Dialog_Help->setObjectName(QStringLiteral("Dialog_Help"));
        Dialog_Help->resize(500, 350);
        gridLayout = new QGridLayout(Dialog_Help);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        textBrowser = new QTextBrowser(Dialog_Help);
        textBrowser->setObjectName(QStringLiteral("textBrowser"));

        gridLayout->addWidget(textBrowser, 1, 0, 1, 1);

        label = new QLabel(Dialog_Help);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        pushButton = new QPushButton(Dialog_Help);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        gridLayout->addWidget(pushButton, 2, 0, 1, 1);


        retranslateUi(Dialog_Help);

        QMetaObject::connectSlotsByName(Dialog_Help);
    } // setupUi

    void retranslateUi(QDialog *Dialog_Help)
    {
        Dialog_Help->setWindowTitle(QApplication::translate("Dialog_Help", "Dialog", Q_NULLPTR));
        textBrowser->setHtml(QApplication::translate("Dialog_Help", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'SimSun'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">USB-HID\345\233\272\344\273\266\345\215\207\347\272\247\350\275\257\344\273\266,\345\210\251\347\224\250\345\205\215\351\251\261\347\232\204HID\345\215\217\350\256\256,\345\215\263\346\217\222\345\215\263\347\224\250</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">\350\257\245\350\275\257\344\273\266\347\224\261\346\255\246\346\261\211\350\267\263\345\205\224\347\247\221\346\212\200OpenRabbit\347\274\226\345\206\231\345\271\266\345\274\200\346\272\220</p>\n"
"<p style=\" margin-top:0px"
                        "; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">\350\257\245\350\275\257\344\273\266\347\232\204\351\200\232\344\277\241\345\215\217\350\256\256\345\217\257\344\273\245\345\234\250\350\267\263\345\205\224\347\247\221\346\212\200\345\256\230\347\275\221www.whtiaotu.com\344\270\213\350\275\275</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">\350\275\257\344\273\266\350\207\252\345\270\246\345\233\272\344\273\266\347\274\226\347\240\201\345\217\221\351\200\201\345\212\237\350\203\275\343\200\201\346\226\207\344\273\266\350\267\257\345\276\204\350\256\260\345\277\206\345\212\237\350\203\275\343\200\201\347\253\257\345\217\243\350\207\252\345\212\250\346\243\200\346\265\213\345\212\237\350\203\275\343\200\201\345\233\272\344\273\266\346\226\207\344\273\266\345\261\225\347\244\272\345\212\237\350\203\275\343\200\201\351\200\232\350\256\257\346\225\260\346\215\256\345\261\225\347\244\272\345"
                        "\212\237\350\203\275\343\200\202</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">\346\224\257\346\214\201\347\233\264\346\216\245\346\213\226\345\212\250\345\233\272\344\273\266\346\226\207\344\273\266\345\210\260\347\252\227\345\217\243\350\277\233\350\241\214\346\211\223\345\274\200</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>", Q_NULLPTR));
        label->setText(QApplication::translate("Dialog_Help", "USB-HID\345\233\272\344\273\266\345\215\207\347\272\247\350\275\257\344\273\266\345\270\256\345\212\251\346\226\207\346\241\243", Q_NULLPTR));
        pushButton->setText(QApplication::translate("Dialog_Help", "\350\277\224\345\233\236\344\270\273\347\225\214\351\235\242", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class Dialog_Help: public Ui_Dialog_Help {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG_HELP_H
