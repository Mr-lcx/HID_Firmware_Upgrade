/********************************************************************************
** Form generated from reading UI file 'dialog_set.ui'
**
** Created by: Qt User Interface Compiler version 5.9.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG_SET_H
#define UI_DIALOG_SET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>

QT_BEGIN_NAMESPACE

class Ui_Dialog_Set
{
public:
    QGridLayout *gridLayout;
    QLabel *label_7;
    QSpacerItem *verticalSpacer;
    QLineEdit *lineEdit_pid;
    QLabel *label;
    QLabel *label_2;
    QLineEdit *lineEdit_max_file_size;
    QLabel *label_6;
    QLineEdit *lineEdit_vid;
    QLabel *label_4;
    QLabel *label_3;
    QLabel *label_5;
    QLineEdit *lineEdit_timeout;
    QSpinBox *spinBox_report_id;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButton_save;
    QPushButton *pushButton_cancel;
    QPushButton *pushButton_default;
    QLabel *label_8;
    QLabel *label_9;
    QLineEdit *lineEdit_getfirmware_url;
    QLineEdit *lineEdit_postfirmware_url;

    void setupUi(QDialog *Dialog_Set)
    {
        if (Dialog_Set->objectName().isEmpty())
            Dialog_Set->setObjectName(QStringLiteral("Dialog_Set"));
        Dialog_Set->resize(448, 424);
        gridLayout = new QGridLayout(Dialog_Set);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label_7 = new QLabel(Dialog_Set);
        label_7->setObjectName(QStringLiteral("label_7"));

        gridLayout->addWidget(label_7, 9, 2, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 9, 0, 1, 1);

        lineEdit_pid = new QLineEdit(Dialog_Set);
        lineEdit_pid->setObjectName(QStringLiteral("lineEdit_pid"));

        gridLayout->addWidget(lineEdit_pid, 2, 2, 1, 1);

        label = new QLabel(Dialog_Set);
        label->setObjectName(QStringLiteral("label"));
        label->setTextFormat(Qt::AutoText);
        label->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label, 0, 0, 1, 3);

        label_2 = new QLabel(Dialog_Set);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 2);

        lineEdit_max_file_size = new QLineEdit(Dialog_Set);
        lineEdit_max_file_size->setObjectName(QStringLiteral("lineEdit_max_file_size"));

        gridLayout->addWidget(lineEdit_max_file_size, 6, 2, 1, 1);

        label_6 = new QLabel(Dialog_Set);
        label_6->setObjectName(QStringLiteral("label_6"));

        gridLayout->addWidget(label_6, 6, 0, 1, 2);

        lineEdit_vid = new QLineEdit(Dialog_Set);
        lineEdit_vid->setObjectName(QStringLiteral("lineEdit_vid"));

        gridLayout->addWidget(lineEdit_vid, 1, 2, 1, 1);

        label_4 = new QLabel(Dialog_Set);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout->addWidget(label_4, 5, 0, 1, 2);

        label_3 = new QLabel(Dialog_Set);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout->addWidget(label_3, 2, 0, 1, 2);

        label_5 = new QLabel(Dialog_Set);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout->addWidget(label_5, 4, 0, 1, 2);

        lineEdit_timeout = new QLineEdit(Dialog_Set);
        lineEdit_timeout->setObjectName(QStringLiteral("lineEdit_timeout"));

        gridLayout->addWidget(lineEdit_timeout, 5, 2, 1, 1);

        spinBox_report_id = new QSpinBox(Dialog_Set);
        spinBox_report_id->setObjectName(QStringLiteral("spinBox_report_id"));

        gridLayout->addWidget(spinBox_report_id, 4, 2, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(5, 5, 5, 5);
        pushButton_save = new QPushButton(Dialog_Set);
        pushButton_save->setObjectName(QStringLiteral("pushButton_save"));

        horizontalLayout->addWidget(pushButton_save);

        pushButton_cancel = new QPushButton(Dialog_Set);
        pushButton_cancel->setObjectName(QStringLiteral("pushButton_cancel"));

        horizontalLayout->addWidget(pushButton_cancel);

        pushButton_default = new QPushButton(Dialog_Set);
        pushButton_default->setObjectName(QStringLiteral("pushButton_default"));

        horizontalLayout->addWidget(pushButton_default);


        gridLayout->addLayout(horizontalLayout, 10, 0, 1, 3);

        label_8 = new QLabel(Dialog_Set);
        label_8->setObjectName(QStringLiteral("label_8"));

        gridLayout->addWidget(label_8, 7, 0, 1, 2);

        label_9 = new QLabel(Dialog_Set);
        label_9->setObjectName(QStringLiteral("label_9"));

        gridLayout->addWidget(label_9, 8, 0, 1, 2);

        lineEdit_getfirmware_url = new QLineEdit(Dialog_Set);
        lineEdit_getfirmware_url->setObjectName(QStringLiteral("lineEdit_getfirmware_url"));

        gridLayout->addWidget(lineEdit_getfirmware_url, 7, 2, 1, 1);

        lineEdit_postfirmware_url = new QLineEdit(Dialog_Set);
        lineEdit_postfirmware_url->setObjectName(QStringLiteral("lineEdit_postfirmware_url"));

        gridLayout->addWidget(lineEdit_postfirmware_url, 8, 2, 1, 1);


        retranslateUi(Dialog_Set);

        QMetaObject::connectSlotsByName(Dialog_Set);
    } // setupUi

    void retranslateUi(QDialog *Dialog_Set)
    {
        Dialog_Set->setWindowTitle(QApplication::translate("Dialog_Set", "Dialog", Q_NULLPTR));
        label_7->setText(QApplication::translate("Dialog_Set", "\344\277\256\346\224\271\345\220\216\350\257\267\351\207\215\345\220\257\350\275\257\344\273\266", Q_NULLPTR));
        label->setText(QApplication::translate("Dialog_Set", "\345\217\202\346\225\260\351\205\215\347\275\256\347\225\214\351\235\242", Q_NULLPTR));
        label_2->setText(QApplication::translate("Dialog_Set", "vendor_id(dec)", Q_NULLPTR));
        label_6->setText(QApplication::translate("Dialog_Set", "\345\233\272\344\273\266\346\234\200\345\244\247\351\225\277\345\272\246(\345\215\225\344\275\215KB)", Q_NULLPTR));
        label_4->setText(QApplication::translate("Dialog_Set", "\350\266\205\346\227\266\346\227\266\351\227\264(\345\215\225\344\275\215100ms)", Q_NULLPTR));
        label_3->setText(QApplication::translate("Dialog_Set", "product_id(dec)", Q_NULLPTR));
        label_5->setText(QApplication::translate("Dialog_Set", "report_id(dec)", Q_NULLPTR));
        pushButton_save->setText(QApplication::translate("Dialog_Set", "\344\277\235\345\255\230", Q_NULLPTR));
        pushButton_cancel->setText(QApplication::translate("Dialog_Set", "\345\217\226\346\266\210", Q_NULLPTR));
        pushButton_default->setText(QApplication::translate("Dialog_Set", "\346\201\242\345\244\215\350\267\263\345\205\224\347\247\221\346\212\200\351\273\230\350\256\244\345\200\274", Q_NULLPTR));
        label_8->setText(QApplication::translate("Dialog_Set", "\345\233\272\344\273\266\344\270\213\350\275\275\345\234\260\345\235\200", Q_NULLPTR));
        label_9->setText(QApplication::translate("Dialog_Set", "\345\233\272\344\273\266\344\270\212\344\274\240\345\234\260\345\235\200", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class Dialog_Set: public Ui_Dialog_Set {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG_SET_H
