#include "webbrowser.h"
#include "ui_webbrowser.h"

WebBrowser::WebBrowser(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WebBrowser)
{
    ui->setupUi(this);


    ui->Web_Browser->setControl(QString::fromUtf8("{8856F961-340A-11D0-A96B-00C04FD705A2}"));
    ui->Web_Browser->dynamicCall("Navigate(const QString&)", "http://www.baidu.com");



}

WebBrowser::~WebBrowser()
{
    delete ui;
}

void WebBrowser::set_url(QString s)
{
    if(s.isEmpty())
        return;
    url = s;
    ui->Web_Browser->dynamicCall("Navigate(const QString&)", url);
}
