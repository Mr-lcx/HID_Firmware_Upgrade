﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include "hidapi.h"


/*************************************************
一：必读：有的WINDOWS上面会缺少msvcr100d.dll文件
（1）下载文件msvcr100d.dll文件到您的桌面上。
（2）将msvcr100d.dll文件放到提示丢失的文件的程序目录下。
（3）如果第2步是行不通的。将文件msvcr100d.dll到系统目录下。
C:\Windows\System （Windows 95/98/Me）
C:\WINNT\System32 （Windows NT/2000）
C:\Windows\System32 （Windows XP, Vista）
C:\Windows\System32 （Windows 7/8/2008r2）
C:\Windows\SysWOW64 （Windows 7/8/2008r2）

msvcr100d.dll放在obj文件中且已经打包进USB_HID_boxed.exe文件中了，如果自己编译的时候提示程序异常结束，
那么很可能是msvcr100d.dll缺失，只需要将msvcr100d.dll复制到C盘指定位置，如果程序已经编译好了，那么只需
要将msvcr100d.dll打包进EXE文件，在目标计算机缺失msvcr100d.dll文件的情况下也能正常运行，如果在目标计算
机运行的时候提示msvcr100d.dll缺失或者hidapi.dll缺失只需要将这两个文件复制到EXE的目录或者复制到C盘

二：hidapi.dll也需要放到debug目录或者Release目录下，打包的时候也需要将其复制并打包进去


笔记:

QT调用LIB库的方法  LIBS += -L$$_PRO_FILE_PWD_/  -lhidapi （在.pro文件里面）

用plainTextEdit加载文件数据要比textEdit快,加载580K的bin文件,plainTextEdit用了2秒，textEdit用了5秒,后面加上选择是否加载的功能,
不加载bin文件到数据接收区,打开文件的速度飞速提升。bin文件可用sublimeText查看

调试的时候可以将USB数据显示到数据接收区，调试完成后没有必要了，QT的TextEdit填充会耽误数据的传输
没有数据填充的时候27K/S，填充数据的时候17K/S

超时时间得根据文件大小MCU烧写flash需要的时间来计算

USB线程可以适当的休眠,下载时候没休眠,占用CPU达到了9%，USB未连接时占用CPU为0

***********************************************/





MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->setWindowTitle("USB固件升级");        //设置窗口标题

    this->setAcceptDrops(true);                 //使能拖动事件接收

    ui->lineEdit_FileName->setFocusPolicy(Qt::NoFocus);  //不可获得焦点,不可编辑,又能查看完整的显示文本

    ui->textEdit_information->setFocusPolicy(Qt::NoFocus);  //不可获得焦点,不可编辑,又能查看完整的显示文本

    ui->textEdit_information->setAcceptDrops(false);        //关闭拖动事件

    ui->plainTextEdit_receive->setAcceptDrops(false);        //关闭拖动事件

    ui->pushButton_StartDownload->setEnabled(false);  //开始下载按钮先关闭

    ui->progressBar_Download_Speed->setValue(0);      //下载进度归0

    myusbthread = new USBThread(this);      //新建USB操作的线程

    //信号与槽的绑定
    connect(myusbthread,SIGNAL(new_data(unsigned char*,unsigned int)),this,SLOT(usbreceivedata(unsigned char*,unsigned int)));
    connect(myusbthread,SIGNAL(usb_connect(QString)),this,SLOT(usbconnect(QString)));
    connect(myusbthread,SIGNAL(usb_disconnect()),this,SLOT(usbdisconnect()));
    connect(myusbthread,SIGNAL(usb_send_error()),this,SLOT(usbsenderror()));


    /* 读取配置文件 */
    Setting set;
    QString str;
    str = set.readSettings("vendor_id");
    if(!str.isEmpty())
        ui->lineEdit_vid->setText(str);
    else
        set.writeSettings("vendor_id",ui->lineEdit_vid->text());
    str = set.readSettings("product_id");
    if(!str.isEmpty())
        ui->lineEdit_pid->setText(str);
    else
        set.writeSettings("product_id",ui->lineEdit_pid->text());

    //USB线程的一些设置
    myusbthread->vid = ui->lineEdit_vid->text().toUShort();
    myusbthread->pid = ui->lineEdit_pid->text().toUShort();
    myusbthread->open_usb();
    myusbthread->start();



    ui->label_url->setOpenExternalLinks(true);      //设置label可以跳转到浏览器
    //ui->label_url->setTextInteractionFlags(Qt::TextSelectableByMouse);  //设置label内容可复制

    ui->label_DevString->setTextInteractionFlags(Qt::TextSelectableByMouse);  //设置label内容可复制

    /* 读取上一次打开文件的路径 */
    file_path = set.readSettings("file_path");

    firmware_send_size = 0;
    firmware_send_index = 0;

    program_default_timeout = DEFAULT_PROGRAM_DEFAULT_TIMEOUT;
    str = set.readSettings("program_default_timeout");
    if(!str.isEmpty())
        program_max_timeout = str.toInt();
    else
        set.writeSettings("program_default_timeout",QString::number(program_default_timeout));
    program_timeout = 0;
    program_max_timeout = program_default_timeout;
    program_status = false;

    max_file_size = DEFAULT_MAX_FILE_SIZE/1024;
    str = set.readSettings("max_file_size");
    if(!str.isEmpty())
        max_file_size = str.toInt();
    else
        set.writeSettings("max_file_size",QString::number((DEFAULT_MAX_FILE_SIZE)/1024));
    max_file_size *= 1024;

    ui->checkBox_loadbin->setChecked(DEFAULT_LOADBIN?true:false);
    str = set.readSettings("loadbin");
    if(!str.isEmpty())
        ui->checkBox_loadbin->setChecked(str.toInt()?true:false);
    else
        set.writeSettings("loadbin",QString::number(DEFAULT_LOADBIN));


    ui->checkBox_loaddata->setChecked(DEFAULT_LOADDATA?true:false);
    str = set.readSettings("loaddata");
    if(!str.isEmpty())
        ui->checkBox_loaddata->setChecked(str.toInt()?true:false);
    else
        set.writeSettings("loaddata",QString::number(DEFAULT_LOADDATA));


    getfirmware_url = set.readSettings("getfirmware_url");
    if(getfirmware_url.isEmpty())
    {
        getfirmware_url = DEFAULT_GETFIRMWARE_URL;
    }
    postfirmware_url = set.readSettings("postfirmware_url");
    if(postfirmware_url.isEmpty())
    {
        postfirmware_url = DEFAULT_POSTFIRMWARE_URL;
    }


    //设置拖拽的下限,防止窗口因拖拽而被隐藏
    ui->splitter->setCollapsible(0,false);
    ui->splitter->setCollapsible(1,false);

    /* 新建一个用于超时计算的定时器 */
    mytimer = new QTimer(this);
    connect(mytimer,SIGNAL(timeout()),this,SLOT(mytimer_timeout()));

    /* 绑定action到槽函数 */
    connect(ui->action_help,&QAction::triggered,this,&MainWindow::on_action_help);
    connect(ui->action_set,&QAction::triggered,this,&MainWindow::on_action_set);
    connect(ui->action_getfirmware,&QAction::triggered,this,&MainWindow::on_action_getfirmware);
    connect(ui->action_postfirmware,&QAction::triggered,this,&MainWindow::on_action_postfirmware);





}

MainWindow::~MainWindow()
{
    delete ui;
}


//窗口关闭事件
void MainWindow::closeEvent(QCloseEvent *event)
{
    /* 存储相关设置 */
    Setting set;
    set.writeSettings("loadbin",QString::number(ui->checkBox_loadbin->isChecked()));
    set.writeSettings("loaddata",QString::number(ui->checkBox_loaddata->isChecked()));

    if(myusbthread->isRunning())
    {
        myusbthread->stop_thread();
        myusbthread->wait();
    }
    event->accept();
}

//拖动事件
void MainWindow::dragEnterEvent(QDragEnterEvent *event)
{
    event->acceptProposedAction();
}



//拖动放下事件
//放下事件
void MainWindow::dropEvent(QDropEvent *event)
{
    qDebug()<<"拖动放下事件";

    QString fileName = event->mimeData()->urls().first().toLocalFile();
    qDebug()<<fileName.right(3).compare("bin");

    if(fileName.right(4).compare(".bin") != 0)
       return;

#if 1
    if(fileName.isEmpty())
        return;

    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly ))
    {
        QMessageBox::warning(this,"error","打开文件失败",QMessageBox::Ok);
        ui->textEdit_information->append("打开文件失败");
        return;
    }


    if(file.size() > max_file_size)
    {
        QMessageBox::warning(this,"错误","固件过大,请选择正确的固件",QMessageBox::Ok);
        ui->textEdit_information->append("固件过大,请选择正确的固件");
        return;
    }
    else if(file.size() <= 0)
    {
        QMessageBox::warning(this,"错误","空的固件",QMessageBox::Ok);
        ui->textEdit_information->append("固件是空的");
        return;
    }
    else
    {
        ui->lineEdit_FileName->setText(fileName);

        Setting set;
        set.writeSettings("file_path",fileName);

        firmware_arry=file.readAll();//读取文件

        int length=firmware_arry.size();//计算长度

        if(length >= 10*1024*1024)
            program_max_timeout += length / 1024;  //1K加一秒

        ui->label_FirmwareSize->setText("固件大小:"+QString::number(length)+"字节");
        if(length > 1024)
            ui->label_FirmwareSize->setText("固件大小:"+QString::number((float)length/1024,'f',1)+"KB   "+QString::number(length)+"字节");

        int i=0;
        QString file_str;

        if(ui->checkBox_loadbin->isChecked())
        {
            for(i=0;i<length;i++)
            {
                QString str;
                str.sprintf("0X%02X", (unsigned char)firmware_arry.at(i));      //转化为十六进制

                file_str.append(str);
                file_str.append("  ");
            }

            ui->plainTextEdit_receive->appendPlainText(file_str);
        }

        ui->pushButton_StartDownload->setText("开始下载");
        ui->textEdit_information->append("读取文件完成...");

    }
    file.close();

    if(myusbthread->get_usbstatus() == USB_CONNECT)
    {
        ui->pushButton_StartDownload->setEnabled(true);

    }
#endif




    event->accept();
}

//收到USB数据
void MainWindow::usbreceivedata(unsigned char *data, unsigned int length)
{

    unsigned int count=0;
    QString receive_str;
    if(ui->checkBox_loaddata->isChecked())
    {
        /*  */
        for(count=0;count<length;count++)
        {
            QString str;
            /* 转化为十六进制 */
            str.sprintf("0X%02X",data[count]);

            /* 将每个数据追加到str里面 */
            receive_str.append(str);
            receive_str.append("  ");

            /* 直接append的话每append一次就会有一个换行 */
            //ui->plainTextEdit_receive->appendPlainText(str+" ");

        }
        /* 将收到的数据转化为十六进制后送到数据接收区显示 */
        ui->plainTextEdit_receive->appendPlainText(receive_str);

        /* 移动滚动条到底部,可以实时观察最新数据 */
        QScrollBar *scrollbar = ui->plainTextEdit_receive->verticalScrollBar();
        if (scrollbar)
        {
            scrollbar->setSliderPosition(scrollbar->maximum());
        }
    }


   /* 非编程状态执行到这里就可以结束了 */
    if(program_status != true)
        return;



    int crc = 0;  //本地计算的CRC值
    int index;   //解析下位机发来的数据编号
    int i;
    unsigned char recv_buf[64] = {0};
    unsigned char send_buf[64] = {0};

    /* 将收到的数据copy出来 */
    memcpy(recv_buf,data,64);

    /* 判断header */
    switch (recv_buf[0])
    {
    case CONNECT_MCU_TO_PC:      /* 握手回复 */

        /* 校验 */
        if(CRC_Check(recv_buf) == 0)
        {
            /* 超时时间清0 */
            program_timeout = 0;

            /* CRC校验通过 */
            /* 握手确认并回复 */
            firmware_send_index = 0;

            /* 记录开始下载的时间 */
            download_start_time = QDateTime::currentSecsSinceEpoch();

            firmware_send_size = firmware_arry.size();

            index = recv_buf[3];
            index = (index<<8) + recv_buf[2];

            qDebug()<<"握手成功";
            ui->textEdit_information->append("握手成功...");
            ui->textEdit_information->append("开始传输固件...");
            /* 开始文件传输 */

            send_buf[0] = FIRMWARE_PC_TO_MCU;
            send_buf[2] = (unsigned char)(firmware_send_index & 0X00FF);
            send_buf[3] = (unsigned char)(firmware_send_index >> 8);
            if(firmware_send_size >= 58)
            {
                send_buf[1] = 58 + 6;
                for(i=0;i<58;i++)
                    send_buf[4+i] = (unsigned char)firmware_arry.at(58*firmware_send_index+i);
                crc = CRC16(send_buf,send_buf[1]-2);
                send_buf[4+i] = crc & 0X00FF;
                i++;
                send_buf[4+i] = crc >> 8;

                firmware_send_size -= 58;

            }
            else if(firmware_send_size > 0)
            {
                send_buf[1] = firmware_send_size + 6;
                for(i=0;i<firmware_send_size;i++)
                    send_buf[4+i] = (unsigned char)firmware_arry.at(58*firmware_send_index+i);
                crc = CRC16(send_buf,send_buf[1]-2);
                send_buf[4+i] = crc & 0X00FF;
                i++;
                send_buf[4+i] = crc >> 8;
                for(i=i;i<58;i++)
                    send_buf[6+i] = 0;

                firmware_send_size = 0;
            }
            myusbthread->send_data(send_buf,64);
            firmware_send_index++;
            ui->progressBar_Download_Speed->setValue((float)firmware_send_index*58/firmware_arry.size()*100);

        }
        break;
    case FIRMWARE_MCU_TO_PC:        /* 固件传输回复 */

        /* 校验 */
        if(CRC_Check(recv_buf) == 0)
        {
            /* 超时时间清0 */
            program_timeout = 0;

            /* CRC校验通过 */
            /* 握手确认并回复 */


            index = recv_buf[3];
            index = (index<<8) + recv_buf[2];


            if(index != firmware_send_index-1)
                break;

            send_buf[0] = FIRMWARE_PC_TO_MCU;
            send_buf[2] = (unsigned char)(firmware_send_index & 0X00FF);
            send_buf[3] = (unsigned char)(firmware_send_index >> 8);
            if(firmware_send_size >= 58)
            {
                send_buf[1] = 58 + 6;
                for(i=0;i<58;i++)
                    send_buf[4+i] = (unsigned char)firmware_arry.at(58*firmware_send_index+i);
                crc = CRC16(send_buf,send_buf[1]-2);
                send_buf[4+i] = crc & 0X00FF;
                i++;
                send_buf[4+i] = crc >> 8;

                firmware_send_size -= 58;

            }
            else if(firmware_send_size > 0)
            {
                send_buf[1] = firmware_send_size + 6;
                for(i=0;i<firmware_send_size;i++)
                    send_buf[4+i] = (unsigned char)firmware_arry.at(58*firmware_send_index+i);
                crc = CRC16(send_buf,send_buf[1]-2);
                send_buf[4+i] = crc & 0X00FF;
                i++;
                send_buf[4+i] = crc >> 8;
                for(i=i;i<58;i++)
                    send_buf[6+i] = 0;

                firmware_send_size = 0;
            }
            else
            {
                break;
            }
            myusbthread->send_data(send_buf,64);
            firmware_send_index++;
            ui->progressBar_Download_Speed->setValue((float)firmware_send_index*58/firmware_arry.size()*100);
            if(ui->progressBar_Download_Speed->value() >= 100)
            {
                /* 此时文件已经传完了,但不代表已经烧写完成,防止用户误以为下载完成,进度条设置为99 */
                ui->progressBar_Download_Speed->setValue(99);
            }

        }
        break;
    case RECVDONE_MCU_TO_PC:       /* 接收完成回复,(非烧写完成) */

        /* 校验 */
        if(CRC_Check(recv_buf) == 0)
        {

            /* 超时时间清0 */
            program_timeout = 0;

            ui->textEdit_information->append("传输完成...");
            /* 记录结束下载的时间 */
            download_end_time = QDateTime::currentSecsSinceEpoch();

            qint64 download_time = download_end_time - download_start_time;
            ui->textEdit_information->append("传输耗时"+QString::number(download_time)+"秒");
            int speed = firmware_arry.size() / download_time;
            if(speed > 1024)
                ui->textEdit_information->append(QString::number((float)speed/1024,'f',2)+"KB/秒");
            else
                ui->textEdit_information->append(QString::number(speed)+"字节/秒");
            ui->textEdit_information->append("请等待设备烧写完成...");

        }
        break;
    case PROGRAMDONW_MCU_TO_PC:          /* 编程烧写完成回复 */

        /* 校验 */
        if(CRC_Check(recv_buf) == 0)
        {
            /* 超时时间清0 */
            program_timeout = 0;

            /* 标记为非更新状态 */
            program_status = false;

            /* 下载完成,超时定时器停止 */
            mytimer->stop();

            /* 记录烧写完成的时间 */
            program_end_time = QDateTime::currentSecsSinceEpoch();

            /* 计算下载和烧写耗时并显示 */
            ui->textEdit_information->append("下载烧写过程耗时"+QString::number(program_end_time-download_start_time)+"秒");

            /* 此时已经烧写完成,进度条设置100% */
            ui->progressBar_Download_Speed->setValue(100);

            ui->textEdit_information->append("烧写完成...");
            ui->textEdit_information->append("本次下载结束");
        }
        break;
    case ERROR_MCU_TO_PC:            /* 报告错误 */

        /* 校验 */
        if(CRC_Check(recv_buf) == 0)
        {
            /* 超时时间清0 */
            program_timeout = 0;

            /* 标记为非更新状态 */
            program_status = false;

            mytimer->stop();

            QMessageBox::warning(this,"error","更新失败",QMessageBox::Ok);
        }
        break;
    default:
        break;
    }


}


//USB连接成功
void MainWindow::usbconnect(QString dev_str)
{
    ui->pushButton_openclose->setText("关闭USB");
    QPixmap connect_pic(":/img/img/usb_connect.png");
    ui->label_constatus_pic->setPixmap(connect_pic);        //图标设置为USB已连接(彩色图标)
    ui->label_DevString->setText(dev_str);                  //显示设备名称
    ui->textEdit_information->append("设备已连接");
    ui->textEdit_information->append("设备名称:"+dev_str);

    ui->pushButton_StartDownload->setText("开始下载");
    if(firmware_arry.size())
    {
        /* USB连接且固件文件已经打开，使能开始下载按钮 */
        ui->pushButton_StartDownload->setEnabled(true);

    }


}

//USB掉线
void MainWindow::usbdisconnect()
{
    ui->pushButton_openclose->setText("打开USB");
    QPixmap disconnect_pic(":/img/img/usb_disconnect.png");
    ui->label_constatus_pic->setPixmap(disconnect_pic);     //图标设置为USB未连接(灰色图标)
    ui->pushButton_StartDownload->setEnabled(false);        //开始下载按钮禁用
    ui->pushButton_StartDownload->setText("开始下载");
    ui->label_DevString->clear();
    ui->textEdit_information->append("设备已断开");
    if(program_status == true)
    {
        QMessageBox::warning(this,"错误","USB连接断开，下载失败",QMessageBox::Ok);
        program_status = false;
        mytimer->stop();
    }
}



//USB发送失败
void MainWindow::usbsenderror()
{
    QMessageBox::warning(this,"错误","USB发送数据异常",QMessageBox::Ok);
    ui->textEdit_information->append("USB发送失败");
    if(program_status == true)
    {
        program_status = false;
        mytimer->stop();
    }
}

void MainWindow::on_pushButton_openclose_clicked()
{
    if(ui->pushButton_openclose->text() == "打开USB")
    {
        myusbthread->vid = ui->lineEdit_vid->text().toUShort();
        myusbthread->pid = ui->lineEdit_pid->text().toUShort();
        myusbthread->open_usb();
        myusbthread->continue_thread();
    }
    else
    {
        myusbthread->pause_thread();
    }
}




//清除接收区
void MainWindow::on_pushButton_clearReceive_clicked()
{
    ui->plainTextEdit_receive->clear();
}

//打开文件
void MainWindow::on_pushButton_OpenFile_clicked()
{
    ui->textEdit_information->append("打开文件...");

    Setting set;
    file_path = set.readSettings("file_path");
    QString fileName = QFileDialog::getOpenFileName(this,("打开固件文件"),file_path,tr("BIN文件(*.bin)"));
    qDebug()<<"文件名:  "+fileName;

    if(fileName.isEmpty())
        return;

    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly ))
    {
        QMessageBox::warning(this,"error","打开文件失败",QMessageBox::Ok);
        ui->textEdit_information->append("打开文件失败");
        return;
    }


    if(file.size() > max_file_size)
    {
        QMessageBox::warning(this,"错误","固件过大,请选择正确的固件",QMessageBox::Ok);
        ui->textEdit_information->append("固件过大,请选择正确的固件");
        return;
    }
    else if(file.size() <= 0)
    {
        QMessageBox::warning(this,"错误","空的固件",QMessageBox::Ok);
        ui->textEdit_information->append("固件是空的");
        return;
    }
    else
    {
        ui->lineEdit_FileName->setText(fileName);
//        while(file.canReadLine())
//            firmware_arry.append(file.readLine().data());

        Setting set;
        set.writeSettings("file_path",fileName);

        firmware_arry=file.readAll();//读取文件

        int length=firmware_arry.size();//计算长度

        if(length >= 10*1024*1024)
            program_max_timeout += length / 1024;  //1K加一秒

        ui->label_FirmwareSize->setText("固件大小:"+QString::number(length)+"字节");
        if(length > 1024)
            ui->label_FirmwareSize->setText("固件大小:"+QString::number((float)length/1024,'f',1)+"KB   "+QString::number(length)+"字节");

        int i=0;
        QString file_str;

        if(ui->checkBox_loadbin->isChecked())
        {
            for(i=0;i<length;i++)
            {
                QString str;
                str.sprintf("0X%02X", (unsigned char)firmware_arry.at(i));      //转化为十六进制

                file_str.append(str);
                file_str.append("  ");
            }

            ui->plainTextEdit_receive->appendPlainText(file_str);
        }

        ui->pushButton_StartDownload->setText("开始下载");
        ui->textEdit_information->append("读取文件完成...");

    }
    file.close();

    if(myusbthread->get_usbstatus() == USB_CONNECT)
    {
        ui->pushButton_StartDownload->setEnabled(true);

    }
}




//开始下载
void MainWindow::on_pushButton_StartDownload_clicked()
{
    ui->progressBar_Download_Speed->setValue(0);
    int length = firmware_arry.size();
    unsigned int pack_length;
    int i=0;

    program_status = true;      /* 标记正在下载 */
    program_timeout = 0;        /* 开始下载,超时时间清0 */
    mytimer->start(100);        /* 定时器周期设置为100ms */

    ui->pushButton_StartDownload->setText("正在下载");
    ui->pushButton_StartDownload->setEnabled(false);
    ui->textEdit_information->clear();
    ui->textEdit_information->append("开始连接设备...");

    /* 握手 */
    unsigned char send_buf[64] = {0};

    for(i=0;i<64;i++)
        send_buf[i] = 0;
    unsigned int crc = 0;
    /* Header */
    send_buf[0] = CONNECT_PC_TO_MCU;
    /* 数据长度 */
    send_buf[1] = 6;  //长度为6
    /* 包长度 */
    pack_length = length/58+(length%58?1:0);
    qDebug()<<"包长度"+QString::number(pack_length);
    send_buf[2] = pack_length & 0X00FF;
    send_buf[3] = pack_length >> 8;
    crc = CRC16(send_buf,send_buf[1]-2);
    send_buf[4] = crc & 0X00FF;
    send_buf[5] = crc >> 8;
    qDebug("CRC16:0X%X",crc);
    /* 调用USB发送数据 */
    myusbthread->send_data(send_buf,64);

}

//定时时间到了
void MainWindow::mytimer_timeout()
{
    if(program_status == true)
        program_timeout++;
    qDebug("time_out:%d",program_timeout);
    if(program_timeout>=program_max_timeout)
    {
        mytimer->stop();
        QMessageBox::warning(this,"错误","USB更新失败,超时",QMessageBox::Ok);
        program_status = false;
        ui->textEdit_information->append("下载超时");
    }
    //mytimer->start(100);
}




//设置
void MainWindow::on_action_set()
{
    Dialog_Set *dialog = new Dialog_Set;
    dialog->exec();
    delete dialog;
}

//帮助
void MainWindow::on_action_help()
{
    Dialog_Help *dialog = new Dialog_Help;
    dialog->exec();
    delete dialog;

}

//获取固件
void MainWindow::on_action_getfirmware()
{
    WebBrowser *browser = new WebBrowser;

    browser->set_url(getfirmware_url);

    browser->setWindowTitle("获取固件");

    browser->show();
}

//上传固件
void MainWindow::on_action_postfirmware()
{
    WebBrowser *browser = new WebBrowser;

    browser->set_url(postfirmware_url);

    browser->setWindowTitle("上传固件");

    browser->show();
}



/**
  * @brief	Calculation CRC
  * @param
  * @retval crc result
  */
unsigned int MainWindow::CRC16 ( unsigned char *arr_buff, unsigned char len)
{

    unsigned int crc=0xFFFF;
    unsigned char i, j;
    for ( j=0; j<len;j++)
    {

        crc=crc ^*arr_buff++;
        for ( i=0; i<8; i++)
        {
            if( ( crc&0x0001) >0)
            {
                crc=crc>>1;
                crc=crc^ 0xa001;
            }
            else
                crc=crc>>1;
        }
    }
    return ( crc);
}

/**
  * @brief	CRC校验
  * @param	buffer-待校验的数据
  * @retval 0-成功 1-失败
  * @note 针对IAP数据协议的校验,请勿移植
  */
unsigned char MainWindow::CRC_Check(unsigned char *buffer)
{
    uint16_t crc = 0;  //本地计算的CRC值
    uint16_t crc1 = 0; //数据包发来的CRC值
    uint8_t len = 0;  //长度

    /* 校验 */
    len = buffer[1];
    crc = CRC16(buffer,len-2);

    crc1 = buffer[len-1];
    crc1 = (crc1<<8) + buffer[len-2];
    if(crc == crc1)
        return 0;
    else
        return 1;

}


