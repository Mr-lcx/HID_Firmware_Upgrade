﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QFileDialog>
#include <QMessageBox>
#include <QDataStream>
#include <QDateTime>
#include <QDesktopServices>
#include <QScrollBar>
#include <QMimeData>

#include "hidapi.h"
#include "usbthread.h"

#include "setting.h"

#include "dialog_set.h"
#include "ui_dialog_set.h"

#include "dialog_help.h"
#include "ui_dialog_help.h"

#include "webbrowser.h"
#include "ui_webbrowser.h"

/* 指令定义 */
#define CONNECT_PC_TO_MCU	0XAA
#define CONNECT_MCU_TO_PC		0XBB
#define FIRMWARE_PC_TO_MCU	0XCC
#define FIRMWARE_MCU_TO_PC	0XDD
#define RECVDONE_MCU_TO_PC	0XEE
#define PROGRAMDONW_MCU_TO_PC	0XFF
#define ERROR_MCU_TO_PC		0X11


/* 配置的默认值 */
#define DEFAULT_MAX_FILE_SIZE  960*1024
#define DEFAULT_VENDOR_ID  1155
#define DEFAULT_PRODUCT_ID  22352
#define DEFAULT_PROGRAM_DEFAULT_TIMEOUT  80
#define DEFAULT_REPORT_ID 0
#define DEFAULT_LOADBIN 0
#define DEFAULT_LOADDATA 0
#define DEFAULT_GETFIRMWARE_URL "http://www.whtiaotu.com/Bison-Board/Firmware_upgrade/download_firmware.php"
#define DEFAULT_POSTFIRMWARE_URL "http://www.whtiaotu.com/Bison-Board/Firmware_upgrade/Firmware_upgrade.html"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void usbreceivedata(unsigned char *data,unsigned int length);  //接收USB数据
    void usbconnect(QString dev_str);                              //USB建立连接
    void usbdisconnect();                                          //USB断开连接
    void usbsenderror();                                           //USB发送失败

    void mytimer_timeout();   //定时器回调函数

    void on_pushButton_openclose_clicked();

    void on_pushButton_clearReceive_clicked();

    void on_pushButton_OpenFile_clicked();

    void on_pushButton_StartDownload_clicked();

    void on_action_set();

    void on_action_help();

    void on_action_getfirmware();

    void on_action_postfirmware();


protected:
    void    closeEvent(QCloseEvent *event);

    void dragEnterEvent(QDragEnterEvent*event); //拖动进入事件
    void dropEvent(QDropEvent*event);           //拖动放下事件

private:
    Ui::MainWindow *ui;


    USBThread *myusbthread;

    QTimer *mytimer;  //开个定时器用于定时

    int program_timeout;    //判断下载是否超时

    int program_default_timeout;  //默认的下载超时时间,用户可配置的

    int program_max_timeout; //程序下载的超时时间,这个变量根据文件大小动态变化
    bool program_status;    //记录当前状态是否在下载

    int max_file_size;  //最大文件大小


    qint64 download_start_time;     //开始下载的时间
    qint64 download_end_time;       //下载完成的时间
    qint64 program_end_time;        //MCU烧写完成的时间

    unsigned char receive_data[2];      //接收数据缓冲

    QByteArray firmware_arry;       //存放读取到的bin文件

    int firmware_send_size;         //待传输固件的长度
    int firmware_length;         //固件的长度
    int firmware_send_index;        //传输固件的数据编号

    QString getfirmware_url;        //下载固件的网址
    QString postfirmware_url;       //上传固件的网址

    unsigned int CRC16 ( unsigned char *arr_buff, unsigned char len);

    unsigned char CRC_Check(unsigned char *buffer);

    QString file_path;      //记录文件路径

};

#endif // MAINWINDOW_H
