#include "dialog_set.h"
#include "ui_dialog_set.h"

#include "mainwindow.h"

Dialog_Set::Dialog_Set(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog_Set)
{
    ui->setupUi(this);

    this->setWindowTitle("参数配置");

    ui->spinBox_report_id->setMaximum(255);
    ui->spinBox_report_id->setMinimum(0);

    Setting set;
    QString str;
    str = set.readSettings("product_id");
    ui->lineEdit_pid->setText(str);
    str = set.readSettings("vendor_id");
    ui->lineEdit_vid->setText(str);
    str = set.readSettings("program_default_timeout");
    ui->lineEdit_timeout->setText(str);
    str = set.readSettings("max_file_size");
    ui->lineEdit_max_file_size->setText(str);
    str = set.readSettings("report_id");
    ui->spinBox_report_id->setValue(str.toInt());
    str = set.readSettings("getfirmware_url");
    ui->lineEdit_getfirmware_url->setText(str);
    str = set.readSettings("postfirmware_url");
    ui->lineEdit_postfirmware_url->setText(str);



    /* 设置输入时候tab键切换焦点的顺序 */
    setTabOrder(ui->lineEdit_vid, ui->lineEdit_pid);
    setTabOrder(ui->lineEdit_pid, ui->spinBox_report_id);
    setTabOrder(ui->spinBox_report_id, ui->lineEdit_timeout);
    setTabOrder(ui->lineEdit_timeout, ui->lineEdit_max_file_size);
    setTabOrder(ui->lineEdit_max_file_size, ui->pushButton_save);
    setTabOrder(ui->pushButton_save, ui->pushButton_cancel);
    setTabOrder(ui->pushButton_cancel, ui->pushButton_default);

}

Dialog_Set::~Dialog_Set()
{
    delete ui;
}





void Dialog_Set::on_pushButton_cancel_clicked()
{
    this->close();
}

//保存设置
void Dialog_Set::on_pushButton_save_clicked()
{
    Setting set;
    QString str;
    int value;
    str = ui->lineEdit_vid->text();
    if(!str.isEmpty())
    {
        set.writeSettings("vendor_id",str);
    }
    str = ui->lineEdit_pid->text();
    if(!str.isEmpty())
    {
        set.writeSettings("product_id",str);
    }
    str = ui->lineEdit_max_file_size->text();
    if(!str.isEmpty())
    {
        set.writeSettings("max_file_size",str);
    }
    str = ui->lineEdit_timeout->text();
    if(!str.isEmpty())
    {
        set.writeSettings("program_default_timeout",str);
    }
    value = ui->spinBox_report_id->value();
    set.writeSettings("report_id",QString::number(value));

    str = ui->lineEdit_getfirmware_url->text();
    if(!str.isEmpty())
    {
        set.writeSettings("getfirmware_url",str);
    }
    str = ui->lineEdit_postfirmware_url->text();
    if(!str.isEmpty())
    {
        set.writeSettings("postfirmware_url",str);
    }

    this->close();

}

//跳兔科技默认设置
void Dialog_Set::on_pushButton_default_clicked()
{
    Setting set;
    QString str;
    int value;

    /* 填充默认值 */
    ui->lineEdit_max_file_size->setText(QString::number(DEFAULT_MAX_FILE_SIZE/1024));
    ui->lineEdit_pid->setText(QString::number(DEFAULT_PRODUCT_ID));
    ui->lineEdit_vid->setText(QString::number(DEFAULT_VENDOR_ID));
    ui->lineEdit_timeout->setText(QString::number(DEFAULT_PROGRAM_DEFAULT_TIMEOUT));
    ui->spinBox_report_id->setValue(DEFAULT_REPORT_ID);
    ui->lineEdit_getfirmware_url->setText(DEFAULT_GETFIRMWARE_URL);
    ui->lineEdit_postfirmware_url->setText(DEFAULT_POSTFIRMWARE_URL);

    /* 保存设置 */
    str = ui->lineEdit_vid->text();
    if(!str.isEmpty())
    {
        set.writeSettings("vendor_id",str);
    }
    str = ui->lineEdit_pid->text();
    if(!str.isEmpty())
    {
        set.writeSettings("product_id",str);
    }
    str = ui->lineEdit_max_file_size->text();
    if(!str.isEmpty())
    {
        set.writeSettings("max_file_size",str);
    }
    str = ui->lineEdit_timeout->text();
    if(!str.isEmpty())
    {
        set.writeSettings("program_default_timeout",str);
    }
    value = ui->spinBox_report_id->value();
    set.writeSettings("report_id",QString::number(value));

    str = ui->lineEdit_getfirmware_url->text();
    if(!str.isEmpty())
    {
        set.writeSettings("getfirmware_url",str);
    }
    str = ui->lineEdit_postfirmware_url->text();
    if(!str.isEmpty())
    {
        set.writeSettings("postfirmware_url",str);
    }

    this->close();
}
